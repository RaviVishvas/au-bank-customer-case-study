package in.aubank.util;

import java.util.Random;

public class AccountNumberGenerator {

	public static int getAccountNumber() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);

		// this will convert any number sequence into 6 character.
		String accountInString = String.format("%06d", number);
		int accontNumber = Integer.parseInt(accountInString);
		return accontNumber;
	}

}
