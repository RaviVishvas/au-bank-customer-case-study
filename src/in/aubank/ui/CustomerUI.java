package in.aubank.ui;

import java.util.Scanner;

import in.aubank.controller.CustomerController;

public class CustomerUI {

	public static void start() {

		CustomerController controller = new CustomerController();

		Scanner scanner = new Scanner(System.in);

		while (true) {

			System.out.println("Please enter your choice");
			System.out.println("1 for Adding a new customer");
			System.out.println("2 for displaying all the customers");
			System.out.println("3 for Searching a customer");
			System.out.println("4  for Deleting  a customer");
			System.out.println("5  for exiting the application");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				controller.add();
				break;

			case 2:
				controller.showAllCustomers();
				break;

			case 3:
				System.out.println("Searching a  customers");
				break;

			case 4:
				System.out.println("Deleting  a  customers");
				break;

			case 5:
				System.out.println("--------- THnk you for using AU Bank Service-------");
				System.exit(0);

			default:
				System.out.println("Please enter a valid option");
				break;
			}

		}

	}

}
