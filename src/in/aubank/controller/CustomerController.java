package in.aubank.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import in.aubank.Customer;
import in.aubank.services.CustomerService;
import in.aubank.services.impl.CustomerListService;

public class CustomerController {

	CustomerService service = new CustomerListService();

	Scanner scanner = new Scanner(System.in);

	public void add() {

		System.out.println("--------- Please enter the customer details --------");

		String name, email, phone;

		System.out.print("Enter Name: ");
		name = scanner.next();

		System.out.print("Enter Email: ");
		email = scanner.next();

		System.out.print("Enter Phone: ");
		phone = scanner.next();

		LocalDate currentDate = LocalDate.now();

		Customer customer = new Customer(name, email, phone, currentDate);
		String result = service.addCustomer(customer);
		System.out.println(result);

	}

	public void showAllCustomers() {
		System.out.println("Display all the customers");
		List<Customer> customerData = service.getAllCustomers();

		for (Customer c : customerData) {
			System.out.println(c);
		}
	}

}
