package in.aubank.repository.impl;

import java.util.ArrayList;
import java.util.List;

import in.aubank.Customer;
import in.aubank.repository.CustomerRepository;

public class CustomerListRepository implements CustomerRepository {

	List<Customer> customerData = new ArrayList<Customer>();

	@Override
	public String addCustomer(Customer customer) {
		if (customerData.add(customer)) {
			return "Customer with id: " + customer.getCustomerId() + " is added successfully";
		} else {
			return "Customer not added successfully";
		}
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerData;
	}

}
