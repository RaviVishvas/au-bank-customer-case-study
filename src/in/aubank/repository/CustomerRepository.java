package in.aubank.repository;

import java.util.List;

import in.aubank.Customer;

public interface CustomerRepository {

	public String addCustomer(Customer customer);

	public List<Customer> getAllCustomers();

}
