package in.aubank;

import java.time.LocalDate;

public class Customer {
	private int customerId;
	private String name = "";
	private String eamil;
	private String phoneNumber;
	private LocalDate accountCreationDate;

	public Customer() {
	}

	public Customer(String name, String eamil, String phoneNumber, LocalDate accountCreationDate) {

		this.name = name;
		this.eamil = eamil;
		this.phoneNumber = phoneNumber;
		this.accountCreationDate = accountCreationDate;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEamil() {
		return eamil;
	}

	public void setEamil(String eamil) {
		this.eamil = eamil;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public LocalDate getAccountCreationDate() {
		return accountCreationDate;
	}

	public void setAccountCreationDate(LocalDate accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}

}
