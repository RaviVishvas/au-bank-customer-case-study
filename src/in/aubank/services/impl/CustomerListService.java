package in.aubank.services.impl;

import java.util.List;
import java.util.Objects;

import in.aubank.Customer;
import in.aubank.repository.CustomerRepository;
import in.aubank.repository.impl.CustomerListRepository;
import in.aubank.services.CustomerService;
import in.aubank.util.AccountNumberGenerator;

public class CustomerListService implements CustomerService {

	CustomerRepository repository = new CustomerListRepository();

	@Override
	public String addCustomer(Customer customer) {
		if (Objects.isNull(customer)) {
			return "Customer Data is blank";
		} else {
			int accountNumber = AccountNumberGenerator.getAccountNumber();
			customer.setCustomerId(accountNumber);
			String result = repository.addCustomer(customer);
			return result;
		}

	}

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return repository.getAllCustomers();
	}

}
