package in.aubank.services;

import java.util.List;

import in.aubank.Customer;

public interface CustomerService {

	public String addCustomer(Customer customer);

	public List<Customer> getAllCustomers();

}
